%%
% RBE3001 - Laboratory 1 
% 
% Instructions
% ------------
% Welcome again! This MATLAB script is your starting point for Lab
% 1 of RBE3001. The sample code below demonstrates how to establish
% communication between this script and the Nucleo firmware, send
% setpoint commands and receive sensor data.
% 
% IMPORTANT - understanding the code below requires being familiar
% with the Nucleo firmware. Read that code first.
clear
clear java
%clear import;
clear classes;
vid = hex2dec('3742');
pid = hex2dec('0007');
disp (vid );
disp (pid);
javaaddpath ../lib/SimplePacketComsJavaFat-0.6.4.jar;
import edu.wpi.SimplePacketComs.*;
import edu.wpi.SimplePacketComs.device.*;
import edu.wpi.SimplePacketComs.phy.*;
import java.util.*;
import org.hid4java.*;
version -java
myHIDSimplePacketComs=HIDfactory.get();
myHIDSimplePacketComs.setPid(pid);
myHIDSimplePacketComs.setVid(vid);
myHIDSimplePacketComs.connect();
% Create a PacketProcessor object to send data to the nucleo firmware
pp = PacketProcessor(myHIDSimplePacketComs); 
try
  SERV_ID = 37;            % we will be talking to server ID 37 on
                           % the Nucleo

  DEBUG   = false;          % enables/disables debug prints

  % Instantiate a packet - the following instruction allocates 64
  % bytes for this purpose. Recall that the HID interface supports
  % packet sizes up to 64 bytes.
  packet = zeros(15, 1, 'single');

  % The following code generates a sinusoidal trajectory to be
  % executed on joint 1 of the arm and iteratively sends the list of
  % setpoints to the Nucleo firmware. 
  %viaPts = [100];
  
  viaPts = [
       20.50 ,   156.50 , -119.25 , 1; % home up
      -237.00 ,  167.75 ,  274.00 , 1; % p1 up
      -212.25 ,  30.75 ,   401.00 , 1; % p1
       352.00 ,  41.75 ,   115.50 , 0; % p2
       336.00 ,  268.25 , -57.25  , 1;  % p2 up
       20.50 ,   156.50 , -119.25 , 1  % home up
      ]
  
  viaPts = [
2157.000000 , -932.500000 , 2171.250000, 1;
2157.000000 , -632.500000 , 2171.250000, 1
2394.000000 , -754.750000 , 2402.500000, 1;
2394.000000 , -854.750000 , 2402.500000, 1;
2414.250000 , -990.750000 , 2539.750000, 1;
2414.250000 , -990.750000 , 2539.750000, 0;
1747.000000 , -958.250000 , 2248.500000, 0;
1747.000000 , -958.250000 , 2248.500000, 1;
1751.250000 , -670.750000 , 2059.750000, 1;
2157.000000 , -632.500000 , 2171.250000, 1
2157.000000 , -932.500000 , 2171.250000, 1
]

  viaPts = transpose(viaPts);

  % Iterate through a sine wave for joint values
  for k = viaPts
      tic
      %incremtal = (single(k) / sinWaveInc);
      packet = zeros(15, 1, 'single');
      pos = k
      packet(1) = pos(1); % Link 1 (0 = home)
      packet(4) = pos(2); % Link 2
      packet(7) = pos(3); % Link 3
      packet(10) = pos(4); % Gripper

     
      % Send packet to the server and get the response
      % Send packet to the server and get the response
      returnPacket = pp.command(SERV_ID, packet);
      toc

      if DEBUG
          disp('Sent Packet:');
          disp(packet);
          disp('Received Packet:');
          disp(returnPacket);
      end
      %THis version will send the command once per call of pp.write
      pp.write(65, packet);
      pause(3);
      returnPacket2=  pp.read(65);
      %this version will start an auto-polling server and read back the
      %current data
      %returnPacket2=  pp.command(65, packet);
      if DEBUG
          disp('Received Packet 2:');
          disp(returnPacket2);
      end
      toc
      pause(0.5) %timeit(returnPacket) !FIXME why is this needed?
      
  end
catch exception
    getReport(exception)
    disp('Exited on error, clean shutdown');
end
% Clear up memory upon termination
pp.shutdown()


toc
